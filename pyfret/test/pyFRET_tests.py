#import sys
#sys.path.append('/home/rebecca/Documents/repos/pyfret_release')

from pyfret import pyFRET as pft

#import pyFRET as pft
import numpy as np
import unittest
import os
import sys

class pyfret_tests(unittest.TestCase):

    def setUp(self):
        self.l1 = [10, 20, 30, 40, 50]
        self.l2 = [50, 40, 30, 20, 10]
        self.test_data = pft.FRET_data(self.l1, self.l2)
        

    def test_init(self):
        self.assertEqual(len(self.test_data.donor), len(self.l1))
        self.assertEqual(len(self.test_data.acceptor), len(self.l2))

    def test_data_from_lists(self):
        self.assertEqual(len(self.test_data.donor), 5)
        self.assertEqual(len(self.test_data.acceptor), 5)

    def test_subtract_bckd(self):
        t1 = np.array(self.l1) - 0.5
        t2 = np.array(self.l2) - 0.5

        self.test_data.subtract_bckd(0.5, 0.5)
        if sys.version_info.major < 3:
            self.assertItemsEqual(self.test_data.donor, t1)
            self.assertItemsEqual(self.test_data.acceptor, t2)
        else:
            self.assertCountEqual(self.test_data.donor, t1)
            self.assertCountEqual(self.test_data.acceptor, t2)

    def test_subtract_crosstalk(self):
        ct_d = 0.05
        ct_a = 0.01
        t1 = ct_d * np.array(self.l1)
        t2 = ct_a * np.array(self.l2)

        t3 = np.array(self.l2) - t1
        t4 = np.array(self.l1) - t2

        self.test_data.subtract_crosstalk(ct_d, ct_a)

        if sys.version_info.major < 3:
            self.assertItemsEqual(self.test_data.donor, t4)
            self.assertItemsEqual(self.test_data.acceptor, t3)
        else:
            self.assertCountEqual(self.test_data.donor, t4)
            self.assertCountEqual(self.test_data.acceptor, t3)

    def test_AND(self):
        thresh_don = 10
        thresh_acc = 10
        select = (np.array(self.l1) > thresh_don) & (np.array(self.l2) > thresh_acc)
        
        self.test_data.threshold_AND(thresh_don, thresh_acc)

        self.assertEqual(len(self.test_data.donor), len(self.test_data.acceptor))
        self.assertEqual(len(self.test_data.donor), len(np.array(self.l1)[select]))
        self.assertEqual(len(self.test_data.acceptor), len(np.array(self.l2)[select]))
        if sys.version_info.major < 3:
            self.assertItemsEqual(self.test_data.donor, np.array(self.l1)[select])
            self.assertItemsEqual(self.test_data.acceptor, np.array(self.l2)[select])
        else:
            self.assertCountEqual(self.test_data.donor, np.array(self.l1)[select])
            self.assertCountEqual(self.test_data.acceptor, np.array(self.l2)[select])

    def test_OR(self):
        thresh_don = 20
        thresh_acc = 20
        select = (np.array(self.l1) > thresh_don) | (np.array(self.l2) > thresh_acc)

        self.test_data.threshold_OR(thresh_don, thresh_acc)

        self.assertEqual(len(self.test_data.donor), len(self.test_data.acceptor))
        self.assertEqual(len(self.test_data.donor), len(np.array(self.l1)[select]))
        self.assertEqual(len(self.test_data.acceptor), len(np.array(self.l2)[select]))

        if sys.version_info.major < 3: 
            self.assertItemsEqual(self.test_data.donor, np.array(self.l1)[select])
            self.assertItemsEqual(self.test_data.acceptor, np.array(self.l2)[select])
        else:
            self.assertCountEqual(self.test_data.donor, np.array(self.l1)[select])
            self.assertCountEqual(self.test_data.acceptor, np.array(self.l2)[select])
    

    def test_SUM(self):
        thresh = 20
        select = (np.array(self.l1) + np.array(self.l2) > thresh)

        self.test_data.threshold_SUM(thresh)

        self.assertEqual(len(self.test_data.donor), len(self.test_data.acceptor))
        self.assertEqual(len(self.test_data.donor), len(np.array(self.l1)[select]))
        self.assertEqual(len(self.test_data.acceptor), len(np.array(self.l2)[select]))

        if sys.version_info.major < 3:
            self.assertItemsEqual(self.test_data.donor, np.array(self.l1)[select])
            self.assertItemsEqual(self.test_data.acceptor, np.array(self.l2)[select])
        else:
            self.assertCountEqual(self.test_data.donor, np.array(self.l1)[select])
            self.assertCountEqual(self.test_data.acceptor, np.array(self.l2)[select])


    def test_PR(self):
        g = 1.0
        E_test = np.array(self.l2).astype(float) / (np.array(self.l2).astype(float) + g * np.array(self.l1).astype(float)) 
        E_trial = self.test_data.proximity_ratio(g)

        self.assertEqual(len(E_trial), len(E_test))

        if sys.version_info.major < 3:
            self.assertItemsEqual(E_trial, E_test)
        else:
            self.assertCountEqual(E_trial, E_test)

    def test_histogram(self):
        E = self.test_data.proximity_ratio()
        fpath = os.path.join("unit_test_results")
        self.test_data.build_histogram(fpath, "test_hist", bin_min=0.0, bin_max=1.0, bin_width=0.02, image = True, imgname = "test_hist", imgtype="png", gauss=False)

        fname = os.path.join("unit_test_results", "test_hist.csv")
        self.assertTrue(os.path.isfile(fname))
        with open(fname, "r") as f:
            test_data = f.read()
        with open(os.path.join("unit_test_results", "std_test_hist.csv"), "r") as f2:
            std_data = f2.read()
        if sys.version_info.major < 3:
            self.assertItemsEqual(test_data, std_data)
        else:
            self.assertCountEqual(test_data, std_data)
        picture = os.path.join("unit_test_results", "test_hist.png")
        self.assertTrue(os.path.isfile(picture))
        if os.path.isfile(picture) and os.path.isfile(fname):
            os.remove(picture)
            os.remove(fname)
        else:
            raise

    def test_gauss_histogram(self):        
        filepath = os.path.join("test_data", "FRET", "DNArun")
        filelist = []
        for i in range(20):
            filelist.append("DNA%04d.dat" %i)
        self.test_data = pft.parse_bin(filepath, filelist)
        self.test_data.threshold_AND(10, 10)
        E = self.test_data.proximity_ratio()
        fpath = os.path.join("unit_test_results")
        self.test_data.build_histogram(fpath, "test_hist", bin_min=0.0, bin_max=1.0, bin_width=0.02, image = True, imgname = "test_hist", imgtype="png", gauss=True, gaussname="gauss_fit")
        picture = os.path.join("unit_test_results", "test_hist.png")
        gauss_pic = os.path.join("unit_test_results", "gauss_fit.png")
        gauss_csv = os.path.join("unit_test_results", "gauss_fit.csv")
        self.assertTrue(os.path.isfile(picture))
        self.assertTrue(os.path.isfile(gauss_pic))
        self.assertTrue(os.path.isfile(gauss_csv))
    
        if os.path.isfile(picture) and os.path.isfile(gauss_csv) and os.path.isfile(gauss_pic):
            os.remove(picture)
            os.remove(gauss_csv)
            os.remove(gauss_pic)
        else:
            raise

    def test_hexplot(self):
        picture = os.path.join("unit_test_results", "test_hex.pdf")
        fpath = os.path.join("unit_test_results")
        self.test_data.make_hex_plot(fpath, "test_hex", imgtype="pdf", labels = ["Donor", "Acceptor"], xmax = None, ymax = None)
        self.assertTrue(os.path.isfile(picture))
        if os.path.isfile(picture) and os.path.isfile(picture):
            os.remove(picture)
        
    def test_3dplot(self):
        picture = os.path.join("unit_test_results", "test_3d.pdf")
        fpath = os.path.join("unit_test_results")
        self.test_data.make_3d_plot(fpath, "test_3d", imgtype="pdf", labels = ["Donor", "Acceptor", "Frequency"])
        self.assertTrue(os.path.isfile(picture))
        if os.path.isfile(picture) and os.path.isfile(picture):
            os.remove(picture)

    def test_binary_read(self):
        filepath = os.path.join("test_data", "FRET", "DNArun")
        filelist = []
        for i in range(20):
            filelist.append("DNA%04d.dat" %i)
        self.test_data = pft.parse_bin(filepath, filelist)
        dd_len = len(self.test_data.donor)
        da_len = len(self.test_data.acceptor)
        self.assertEqual(dd_len, da_len)
        self.assertEqual(dd_len, 10000*20)

    def test_csv_read(self):
        filepath = os.path.join("test_data", "FRET")
        self.test_data = pft.parse_csv(filepath, ["test.csv"])
        dd_len = len(self.test_data.donor)
        da_len = len(self.test_data.acceptor)
        self.assertEqual(dd_len, da_len)

    def test_APBS(self):
        filepath = os.path.join("test_data", "FRET", "DNArun")
        filelist = []
        for i in range(20):
            filelist.append("DNA%04d.dat" %i)
        self.test_data = pft.parse_bin(filepath, filelist)
        self.tests_bursts = self.test_data.APBS(10, 50, 50)
        blen = len(self.tests_bursts.burst_len)
        b_don = len(self.tests_bursts.donor)
        self.assertEqual(blen, b_don)

    def test_DCBS(self):
        filepath = os.path.join("test_data", "FRET", "DNArun")
        filelist = []
        for i in range(20):
            filelist.append("DNA%04d.dat" %i)
        self.test_data = pft.parse_bin(filepath, filelist)
        self.tests_bursts = self.test_data.DCBS(10, 50, 50)
        blen = len(self.tests_bursts.burst_len)
        b_don = len(self.tests_bursts.donor)
        self.assertEqual(blen, b_don)

    def test_burst_plot(self):
        picture = os.path.join("unit_test_results", "test_bursts.pdf")
        fpath = os.path.join("unit_test_results")
        filepath = os.path.join("test_data", "FRET", "DNArun")
        filelist = []
        for i in range(20):
            filelist.append("DNA%04d.dat" %i)
        self.test_data = pft.parse_bin(filepath, filelist)
        self.tests_bursts = self.test_data.DCBS(10, 50, 50)
        self.tests_bursts.scatter_intensity(fpath, "test_bursts", imgtype="pdf", labels = ["Duration", "Intensity"])
        self.assertTrue(os.path.isfile(picture))
        if os.path.isfile(picture) and os.path.isfile(picture):
            os.remove(picture)


        
if __name__ == '__main__':
    unittest.main() 