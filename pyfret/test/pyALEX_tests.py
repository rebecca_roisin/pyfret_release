from __future__ import print_function
#import sys
#sys.path.append('/home/rebecca/Documents/repos/pyfret_release')

from pyfret import pyALEX as pyx

#import pyALEX as pyx
import numpy as np
import unittest
import os
import sys

class pyfret_tests(unittest.TestCase):

    def setUp(self):
        self.testD_D = [10, 20, 30, 40, 50]
        self.testD_A = [1, 2, 2, 1, 0]
        self.testA_D = [8, 18, 2, 1, 37]
        self.testA_A = [50, 40, 2, 1, 50]
        self.test_data = pyx.ALEX_data(self.testD_D, self.testD_A, self.testA_D, self.testA_A)


    def test_init(self):
        self.assertEqual(len(self.test_data.D_D), 5)
        self.assertEqual(len(self.test_data.D_A), 5)
        self.assertEqual(len(self.test_data.A_D), 5)
        self.assertEqual(len(self.test_data.A_D), 5)

    def test_subtract_bckd(self):
        bckDD = 0.5
        bckDA = 0.1
        bckAD = 0.5
        bckAA=0.8
        test_DD = np.array(self.testD_D) - bckDD
        self.test_data.subtract_bckd(bckDD, bckDA, bckAD, bckAA)
        self.assertEqual(len(self.test_data.D_D), 5)
        self.assertEqual(len(self.test_data.D_A), 5)
        self.assertEqual(len(self.test_data.A_D), 5)
        self.assertEqual(len(self.test_data.A_A), 5)
        if sys.version_info.major < 3:
            self.assertItemsEqual(test_DD, self.test_data.D_D)
        else:
            self.assertCountEqual(test_DD, self.test_data.D_D)

    def test_thresholder(self):
        self.test_data.thresholder(15, 15)
        self.assertEqual(len(self.test_data.D_D), len(self.test_data.A_A))

    def test_stoichiometry(self):
        S = self.test_data.stoichiometry()
        self.assertEqual(len(self.test_data.D_D), len(S))

    def test_PR(self):
        g = 1.0
        E_test = np.array(self.testA_D).astype(float) / (np.array(self.testA_D).astype(float) + g * np.array(self.testD_D).astype(float)) 
        E_trial = self.test_data.proximity_ratio(g)

        self.assertEqual(len(E_trial), len(E_test))
        if sys.version_info.major < 3:
            self.assertItemsEqual(E_trial, E_test)
        else:
            self.assertCountEqual(E_trial, E_test)

    def test_stoich_selection(self):
        S_start = self.test_data.stoichiometry()
        self.assertFalse((S_start.all() > 0.3) & (S_start.all() < 0.8))
        S_end = self.test_data.stoichiometry_selection(S_start, 0.3, 0.8)
        self.assertTrue(S_start.all() > 0.3)
        self.assertEqual(len(S_end), len(self.test_data.D_D))

    def test_crosstalk(self):
        F_old = np.array(self.testA_D)
        self.test_data.subtract_crosstalk(0.05, 0.08)
        for i, j in zip(F_old, self.test_data.A_D):
            self.assertTrue(i > j)

    def test_binary_read(self):
        filepath = os.path.join("test_data", "ALEX")
        filelist = ["test_ALEX.csv"]
        self.test_data = pyx.parse_csv(filepath, filelist)
        D_D_len = len(self.test_data.D_D)
        A_A_len = len(self.test_data.A_A)
        self.assertEqual(D_D_len, A_A_len)
 

    def test_csv_read(self):
        filepath = os.path.join("test_data", "ALEX", "HJ_BHR_gel+noGR_10-4")
        filelist = []
        for i in range(20):
            filelist.append("DNA%04d.dat" %i)
        self.test_data = pyx.parse_bin(filepath, filelist)
        d_len = len(self.test_data.D_D)
        a_len = len(self.test_data.A_A)
        self.assertEqual(d_len, a_len)
        self.assertEqual(d_len, 10000*20)

    def test_scatter_hist(self):
        filepath = os.path.join("test_data", "ALEX", "5M")
        filelist = []
        outdir = os.path.join("unit_test_results")
        pic_name = "test_scatter"
        for i in range(20):
            filelist.append("BFP%04d.dat" %i)
        self.test_data = pyx.parse_bin(filepath, filelist)
        self.test_data.subtract_bckd(0.5, 0.1, 0.5, 0.8)
        Smin = 0.3
        Smax = 0.9
        T_D = 20
        T_A = 20
        self.test_data.thresholder(T_D, T_A)
        self.test_data.scatter_hist(Smin, Smax, save=True, filepath=outdir, imgname=pic_name, imgtype="pdf")
        pic = ".".join([pic_name, "pdf"])
        picture = os.path.join(outdir, pic)
        print(picture)
        self.assertTrue(os.path.isfile(picture))
        if os.path.isfile(picture) and os.path.isfile(picture):
            os.remove(picture)

    def test_histogram(self):
        E = self.test_data.proximity_ratio()
        fpath = os.path.join("unit_test_results")
        self.test_data.build_histogram(fpath, "test_hist_ALEX", bin_min=0.0, bin_max=1.0, bin_width=0.02, image = True, imgname = "test_hist_ALEX", imgtype="png", gauss=False)

        fname = os.path.join("unit_test_results", "test_hist_ALEX.csv")
        self.assertTrue(os.path.isfile(fname))
        with open(fname, "r") as f:
            test_data = f.read()
        with open(os.path.join("unit_test_results", "std_test_hist_ALEX.csv"), "r") as f2:
            std_data = f2.read()
        if sys.version_info.major < 3:    
            self.assertItemsEqual(test_data, std_data)
        else:
            self.assertCountEqual(test_data, std_data)
        picture = os.path.join("unit_test_results", "test_hist_ALEX.png")
        self.assertTrue(os.path.isfile(picture))
        if os.path.isfile(picture) and os.path.isfile(fname):
            os.remove(picture)
            os.remove(fname)
        else:
            raise

    def test_gauss_histogram(self):        
        filepath = os.path.join("test_data", "ALEX", "5M")
        filelist = []
        for i in range(20):
            filelist.append("BFP%04d.dat" %i)
        self.test_data = pyx.parse_bin(filepath, filelist)
        self.test_data.thresholder(15, 15)
        E = self.test_data.proximity_ratio()
        outpath = os.path.join("unit_test_results")
        self.test_data.build_histogram(outpath, "test_hist_ALEX", bin_min=0.0, bin_max=1.0, bin_width=0.02, image = True, imgname = "test_hist_ALEX", imgtype="png", gauss=True, gaussname="gauss_fit_ALEX")
        picture = os.path.join("unit_test_results", "test_hist_ALEX.png")
        gauss_pic = os.path.join("unit_test_results", "gauss_fit_ALEX.png")
        gauss_csv = os.path.join("unit_test_results", "gauss_fit_ALEX.csv")
        self.assertTrue(os.path.isfile(picture))
        self.assertTrue(os.path.isfile(gauss_pic))
        self.assertTrue(os.path.isfile(gauss_csv))
    
        if os.path.isfile(picture) and os.path.isfile(gauss_csv) and os.path.isfile(gauss_pic):
            os.remove(picture)
            os.remove(gauss_csv)
            os.remove(gauss_pic)
        else:
            raise

    def test_APBS(self):
        filepath = os.path.join("test_data", "ALEX", "5M")
        filelist = []
        for i in range(20):
            filelist.append("BFP%04d.dat" %i)
        self.test_data = pyx.parse_bin(filepath, filelist)
        self.tests_bursts = self.test_data.APBS(10, 50, 50)
        blen = len(self.tests_bursts.burst_len)
        b_don = len(self.tests_bursts.D_D)
        self.assertEqual(blen, b_don)

    def test_DCBS(self):
        filepath = os.path.join("test_data", "ALEX", "5M")
        filelist = []
        for i in range(20):
            filelist.append("BFP%04d.dat" %i)
        self.test_data = pyx.parse_bin(filepath, filelist)
        self.tests_bursts = self.test_data.DCBS(10, 50, 50)
        blen = len(self.tests_bursts.burst_len)
        b_don = len(self.tests_bursts.D_D)
        self.assertEqual(blen, b_don)

    def test_burst_plot(self):
        picture = os.path.join("unit_test_results", "test_bursts_ALEX.pdf")
        fpath = os.path.join("unit_test_results")
        filepath = os.path.join("test_data", "ALEX", "5M")
        filelist = []
        for i in range(20):
            filelist.append("BFP%04d.dat" %i)
        self.test_data = pyx.parse_bin(filepath, filelist)
        self.tests_bursts = self.test_data.DCBS(10, 50, 50)
        self.tests_bursts.scatter_intensity(fpath, "test_bursts_ALEX", imgtype="pdf", labels = ["Duration", "Intensity"])
        self.assertTrue(os.path.isfile(picture))
        if os.path.isfile(picture) and os.path.isfile(picture):
            os.remove(picture)


if __name__ == '__main__':
    unittest.main() 

