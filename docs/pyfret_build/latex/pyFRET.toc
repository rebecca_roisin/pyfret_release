\select@language {english}
\contentsline {chapter}{\numberline {1}pyFRET Tutorial}{3}{chapter.1}
\contentsline {section}{\numberline {1.1}Installing pyFRET}{3}{section.1.1}
\contentsline {subsection}{\numberline {1.1.1}Getting Python}{3}{subsection.1.1.1}
\contentsline {subsection}{\numberline {1.1.2}Getting Anaconda}{3}{subsection.1.1.2}
\contentsline {subsection}{\numberline {1.1.3}Getting Scikit learn}{4}{subsection.1.1.3}
\contentsline {subsection}{\numberline {1.1.4}Getting pyFRET}{4}{subsection.1.1.4}
\contentsline {section}{\numberline {1.2}Using pyFRET}{4}{section.1.2}
\contentsline {section}{\numberline {1.3}Using pyFRET.pyFRET}{5}{section.1.3}
\contentsline {section}{\numberline {1.4}Using pyFRET.pyALEX}{7}{section.1.4}
\contentsline {section}{\numberline {1.5}Using The Burst Search Algorithms}{10}{section.1.5}
\contentsline {section}{\numberline {1.6}RASP: Recurrence Analysis of Single Particles}{12}{section.1.6}
\contentsline {section}{\numberline {1.7}The Sample Data}{13}{section.1.7}
\contentsline {chapter}{\numberline {2}pyFRET Reference}{15}{chapter.2}
\contentsline {chapter}{\numberline {3}pyALEX Reference}{21}{chapter.3}
\contentsline {chapter}{\numberline {4}Indices and tables}{27}{chapter.4}
\contentsline {chapter}{Python Module Index}{29}{section*.39}
\contentsline {chapter}{Index}{31}{section*.40}
