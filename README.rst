=======
pyFRET
=======

----------------
What is pyFRET?
----------------

pyFRET is a python package for analysis of data from single-molecule fluorescence (smFRET) experiments using confocal laser microscopy.
pyFRET provides methods for:

* parsing files
* burst selection and denoising
* FRET Efficiency calculation
* data visualisation
* Gaussian fits of smFRET histograms

pyFRET supports analysis of data collected using both continuous (FRET) and alternating excitation (ALEX).

pyFRET was written by Rebecca R. Murphy (email: rebeccaroisin@gmail.com) and is licenced under an OSI-compliant BSD licence.
Please see LICENSE.txt for the text of the license.

-------------------------
How is pyFRET structured?
-------------------------
pyFRET is one module. It contains two submodules:

* pyFRET for continuous excitation data
* pyALEX for alternating excitation data


---------------------
How do I use pyFRET?
---------------------

pyFRET is on the Python Package Index (PyPI). From your terminal, it can be installed using "pip install pyFRET".
To use the library functions in your code, you can use "from pyFRET import pyFRET" and "from pyFRET import pyALEX".

Detailed installation instructions can be found in the online tutorial_
    .. _tutorial: http://pyfret.readthedocs.org/en/latest/tutorial.html

Detailed usage instructions can be found in the pyFRET_ and pyALEX_ references, in the online tutorial_ or in the docs folder.
    .. _pyFRET: http://pyfret.readthedocs.org/en/latest/FRET_reference.html
    .. _pyALEX: http://pyfret.readthedocs.org/en/latest/ALEX_reference.html 
    .. _tutorial: http://pyfret.readthedocs.org/en/latest/tutorial.html


Example code, including configuration files, for complete data analysis using pyFRET can be found in the bin folder.

----------------------------------
Specifically, what does pyFRET do?
----------------------------------

Currently, for simple FRET data, pyFRET supports: 

* file parsing for .csv and binary (.dat) files
* parsing lists of photon counts into data objects
* All Photons Burst Search (APBS)
* Dual Channel Burst Search (DCBS)
* AND, SUM and OR thresholding
* calculating proximity ratio (including optional gamma-factor correction)
* building and plotting proximity ratio histograms, including export as .csv files
* 3D plots of smFRET data
* scatterplots for smFRET data
* Gaussian fitting using a single gaussian peak, including export of fits as .csv files
* Recurrence Analysis of Single Particles, as implemented in Hoffmann et al. Phys Chem Chem Phys. 2011 13(5):1857-1871.

For ALEX data, pyFRET supports: 

* file parsing for .csv and binary files
* parsing lists of photon counts into data objects
* All Photons Burst Search (APBS)
* Dual Channel Burst Search (DCBS)
* calculation of the Proximity Ratio and Stoichiometry (including optional gamma-factor correction)
* event selection using calculated stoichiometry
* building and plotting proximity ratio histograms, including export as .csv files
* construction of scatter plot with projections of E and S
* Gaussian fitting using a single gaussian peak, including export of fits as .csv files

Currently, pyFRET **does not** implement:

* probability distribution analysis to denoise fluorescence bursts
* Gaussian fitting of multiple populations
* support for multi-parameter fluorescence detection.

pyFRET currently does not have a parser for data collected using PicoQuant instrumentation.

These are all important features, which it would be very useful to add. If you would like to see these features added, email me (rebeccaroisin@gmail.com), or contribute a patch. 

----------------------
How do I get in touch?
----------------------

pyFRET is hosted on Bitbucket_ in a public code repository_. It's just here_
    .. _Bitbucket: https://bitbucket.org/
    .. _repository: https://bitbucket.org/rebecca_roisin/pyfret_release
    .. _here: https://bitbucket.org/rebecca_roisin/pyfret_release 

If you find a bug, go to the issue tracker_  and add an issue, including a description of the problem you have found.
    .. _tracker: https://bitbucket.org/rebecca_roisin/pyfret_release/issues
    
You can also create an issue if there is a feature that you would like to see added to the pyFRET library.

If you would like to contribute a patch or feature to the code, please clone the repository, create a new local branch and make your changes locally. Run the unit tests to check that you didn't break anything and then push back to the main branch. If you don't feel confident doing that, add an issue or email me and we can fix it together.
